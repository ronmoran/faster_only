from __future__ import absolute_import
__author__ = 'hagay'
import re
import os
import ssl
from celery import Celery

if os.environ.get('CELERY_MON'):
    celery_modules = []

elif os.environ.get("VBRAND_FASTER"):
    celery_modules = ['algo_manager.general_tasks', 'algo_manager.faster_tasks']

else:
    celery_modules = ['algo_manager.general_tasks']


def call_on_init():
    try:
        from lib import recover_state
        recover_state.taas()
    except ImportError:
        pass


class CeleryApp(Celery):
    def on_init(self):
        call_on_init()


broker_use_ssl = {'cert_reqs': ssl.CERT_REQUIRED} if "5671" in os.environ.get("CELERY_BROKER", "") else None
app = CeleryApp('algo_manager',
                broker=os.environ.get("CELERY_BROKER"),
                backend=os.environ.get("CELERY_BROKER"),
                include=celery_modules)


def worker_remote_control(cmd):
    app.control.broadcast(cmd)


# Optional configuration, see the application user guide.
app.conf.update(
    CELERY_TASK_RESULT_EXPIRES=3600,
    CELERYD_PREFETCH_MULTIPLIER=1,
    CELERYD_POOL_RESTARTS=True,
    BROKER_POOL_LIMIT=700,
    # TODO uncomment this with celery4
    # task_protocol=1,
    BROKER_USE_SSL=broker_use_ssl

    # CELERY_ROUTES={"falgo_manager.video_tasks.video_processing": "processing",
    #                "algo_manager.video_tasks.video_detect_prepare": "main_gate"
    #                }
)


if __name__ == '__main__':
    app.start()
