from __future__ import absolute_import
import os
import sys
import time
from celery.worker.control import Panel

import config
from algo_manager.celery import app
from lib import datasets, hostname, s3
from lib.processor import splited_task

this_dir = os.path.dirname(__file__)

s3con = s3.get_conn('vbrand-video-in') if config.file_protocol == 's3' else None


@app.task
def do_cnn(dataset, vid, image_names, env='', threshold=None):
    print ("processing(cnn) %s/%s" % (vid, image_names))

    global caffe_net
    start_time = time.time()
    if dataset not in caffe_net:
        caffe_net[dataset] = datasets.get(dataset, env)
    if not caffe_net[dataset]:
        return dict(ip=hostname.ip, ok=False, data="Error getting caffe net object for dataset: %s" % dataset,
                    elappsed=time.time() - start_time)

    image_names = [image_name.replace('.jpg', '') for image_name in image_names]
    images = []
    if config.file_protocol == 's3':
        for image_name in image_names:
            s3_file = "%s/%s.jpg" % (vid, image_name)
            image = "%s/class_image/%s_%s.jpg" % (config.LOCAL_STORAGE,vid, image_name)
            images.append(image)
            s3con.download(s3_file, image)
    else:
        images = ["%s/%s/%s.jpg" % (config.nfs_media_dir,vid, image_name) for image_name in image_names]

    rets = {}
    for image in images:
        ret = caffe_net[dataset].classify_image(image, remote=False, threshold=threshold)
        rets[image] = ret
    elappsed_time = time.time() - start_time
    return dict(ok=True, data=rets, elappsed=elappsed_time, ip=hostname.ip)


@Panel.register
def reread_databases(_):
    from lib import datasets

    print ("....reread_databases...")
    datasets.need_db_refresh()
    return dict(ok="reloading ended successfully")


@app.task
def do_rcnn(dataset, vid, image_names, mode, options, svm_active=0, env=''):
    print("processing(rcnn) %s/%s mode=%s" % (vid, image_names, mode))
    simage_names = [image_name.replace('.jpg', '') for image_name in image_names]
    start_time = time.time()
    t = splited_task.RcnnServerThread(dataset, vid, simage_names, mode, svm_active)
    t.set_env(env)
    t.set_objects(s3con)
    try:
        t.run()
    except:
        if t.has_exc():
            raise t.get_exc()[0], t.get_exc()[1], t.get_exc()[2]
        else:
            raise sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2]
    finally:
        t.remove_exc()
    elappsed_time = round(time.time() - start_time, 1)
    print ("do_rcnn overall time = %s" % elappsed_time)
    return dict(ok=t.status, data=t.get_result(), elappsed=t.elapsed_time, ip=hostname.ip)


@app.task
def do_faster(dataset, vid, image_names, mode, options, svm_active=0, env=''):
    print ("processing(faster) %s/%s mode=%s models=%s options=%s" % (vid, image_names, mode, dataset, options))
    simage_names = [image_name.replace('.jpg', '') for image_name in image_names]
    start_time = time.time()
    t = splited_task.FasterServerThread(dataset, vid, simage_names, mode, svm_active)
    t.set_options(options)
    t.set_env(env)
    t.set_objects(s3con)
    try:
        t.run()
    except:
        if t.has_exc():
            raise t.get_exc()[0], t.get_exc()[1], t.get_exc()[2]
        else:
            raise sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2]
    finally:
        t.remove_exc()
    elapsed_time = round(time.time() - start_time, 1)
    print("do_rcnn overall time = %s" % elapsed_time)
    return dict(ok=t.status, data=t.get_result(), elappsed=t.elapsed_time, ip=hostname.ip)


def system_cmd(cmd, debug=True):
    if debug:
        print ("=="*20)
        print (cmd)
        print ("=="*20)
    return os.popen(cmd).read()


@Panel.register
def restart_services():
    print("....restart...")
    system_cmd("%s/../scripts/restart_celery.sh" % this_dir)
    return dict(ok="restart ended successfully")
