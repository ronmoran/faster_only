from __future__ import print_function
import time
import sys
from lib import hostname
from lib.processor import faster_checks
from .celery import app

try:
    from .get_logger import get_logger
    logger = get_logger()
except ImportError:
    # Intellij's nasty inspections
    get_logger = None

    class logger(object):
        @staticmethod
        def info(info_str):
            print(info_str)
            sys.stdout.flush()

# NOTE: Only use logger.info


@app.task
def check_connection():
    logger.info("----------- check_connection --------------")
    return dict(status=True, msg="OK", elappsed=0, ok=True)


@app.task
def test_response(id, delay):
    start_time = time.time()
    time.sleep(delay)
    logger.info("Run test_response ..%s.." % id)
    elappsed_time = round(time.time() - start_time, 1)
    return dict(ok=True, data=id, elappsed=elappsed_time, ip=hostname.name)


@app.task
def check_err():
    raise Exception


@app.task
def do_atom(a, b):
    logger.info("do_atom task: a=%s , b= %s" % (a, b))
    start_time = time.time()
    ret = a + b
    elappsed_time = round(time.time() - start_time, 1)
    return dict(ok=True, data=str(ret), elappsed=elappsed_time, ip=hostname.name)


@app.task
def check_faster(models_names):
    """
    Add faster checkup here. Every check up has to return True if it passes or False otherwise.
    If a checkup fails, raise a proper exception. If everything passes, return True
    """
    has_models = faster_checks.check_model_files(models_names)
    if not has_models:
        raise ValueError("model %s not visible from GPU, check NFS")
    else:
        return True
