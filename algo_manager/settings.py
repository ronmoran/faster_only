"""
Django settings for algo_manager project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
#STATIC_ROOT = 'staticfiles'
#STATIC_URL = '/static/'



STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*sli2!#6ym93+)d_ug-nmt7b8gyccgd)qtc9l!*c09-aeb6ncp'
processor_exist = os.environ.get("PROCESSING_EXIST")
try:
    debug_mode = bool(int(os.environ.get("DEBUG_MODE")))
except:
    debug_mode = False
# SECURITY WARNING: don't run with debug turned on in production!
#DEBUG = TEMPLATE_DEBUG = False

DEBUG = debug_mode #not bool(processor_exist)
#TEMPLATES = [dict(OPTIONS=dict(debug=True))]
#TEMPLATES[0]['OPTIONS']['debug'] = True


CORS_ORIGIN_ALLOW_ALL = True

# Application definition

#gpu_id = utils.dicover_gpu_by_task_id()

DIRS = (os.path.join(os.path.dirname(__file__), '..', 'templates').replace('\\', '/'),)
MEDIA_ROOT = os.path.join(DIRS[0], 'media')


INSTALLED_APPS = ['management', 'django_mongoengine']


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'oauth2_provider.backends.OAuth2Backend'
)


OAUTH2_PROVIDER = {
    'SCOPES': {
        'read': 'Read scope',
        'write': 'Write scope',
    },
    'EXPIRE_CODE_DELTA': 120
    #'CLIENT_ID_GENERATOR_CLASS': 'oauth2_provider.generators.ClientIdGenerator',

}



#SESSION_COOKIE_SECURE = True

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

try:
    #from algo_manager import env
    db_name = os.environ.get("DB_NAME")
    serve_only_v2 = bool(int(os.environ.get("SERVE_ONLY_V2")))
except:
    db_name = os.environ.get("DB_NAME")
    serve_only_v2 = False

from lib import hostname

if hostname.ip != 'develop_station':
    try:
        env_name = os.environ.get("ENV")
        SESSION_COOKIE_NAME = env_name
    except:
        env_name = 'production'

DATABASES = {

    'default': {"ENGINE": "django.db.backends.dummy"}
}

MONGODB_DATABASES = {
    "default": {
        "name": db_name,
        "host": os.environ["DB_HOSTNAME"],
        "tz_aware": True
    }
}
# todo use memcached
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        # 'LOCATION': 'localhost:11211',
        # 'TIMEOUT': 500,
        # 'BINARY': True,
        # 'OPTIONS': {'TCP_NODELAY': True}
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

TELEGRAM_ENABLE = True

SERVE_ONLY_V2 = serve_only_v2
