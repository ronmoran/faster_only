__author__ = 'hagay'
import json
import os
from lib.nets import rcnn_classes
from lib import hostname


def _is_processor():
    return "faster" in os.environ.get("VBRAND_SERVICES", [])


if _is_processor():
    from lib.algo.models import cnn

    # TODO faces
    # from lib.nets import new_face_client

if _is_processor():
    from lib.nets import frcnn_net
    # import gop_service

import cv2

ss_obj = None

refresh_model_file="/tmp/refresh_model"

this_dir = os.path.dirname(__file__)

def get_no_of_gpu():
    if not _is_processor():
        return 0
    venv = os.environ["ENV"]
    no_gpu = int(os.popen("%s/../scripts/get_no_of_gpu.sh %s" % (this_dir, venv)).read().strip())
    return no_gpu

class DataSet(object):
    def __init__(self):
        self.clasifier = None
        self.gpu_id = None
        self.source_clasifier = None
        self.recog_clasifier = None
        self.recog2_clasifier = None
        self.fourth_model = None

    def load(self):
        pass

    def set(self, ds, env=''):
        self.ds = ds
        self.env = env if env != None else ''

    def set_gpu(self, gpu_id):
        self.gpu_id = gpu_id

    def set_op_type(self, op_type):
        self.op_type = op_type

    def get_name(self):
        return self.clasifier.get_net_name()

    def classify_image(self, image, remote=False, threshold=0.05):
        return self.clasifier.classify_image(image, remote, threshold=threshold)

    def reload_classes(self):
        print ("reload_classes: ds=%s" % self.ds)
        if ',' not in self.ds and not self.source_clasifier and self.__class__ == FasterDS:
            self._reload_classes()
        else:
            dss = self.ds.split(',')
            first_model = dss[0].strip()
            self._reload_classes(first_model)
            if len(dss) > 1:
                recog_model = dss[1].strip()
                if recog_model and self.recog_clasifier and type(self.recog_clasifier) != cnn.ImagenetClassifier:
                    self._reload_classes(recog_model, self.recog_clasifier)

            if len(dss) > 2:
                source_model = dss[2].strip()
                if source_model and self.source_clasifier:
                    self._reload_classes(source_model, self.source_clasifier)

            if len(dss) > 3:
                recog2_model = dss[3].strip()
                if recog2_model and self.recog2_clasifier:
                    self._reload_classes(recog2_model, self.recog2_clasifier)



    def _reload_classes(self, ds_name=None, s_classifier=None):
        if self.__class__ != FasterDS or type(self.clasifier) == cnn.ImagenetClassifier:
            print ("Skipping _reload_classes for model=%s" % ds_name)
            return
        classifier = self.clasifier if not s_classifier else s_classifier
        ds = self.ds if not ds_name else ds_name
        filtered_classes = rcnn_classes.get_filtered_classes(ds)
        all_claases_in_model = rcnn_classes.get_classes(ds)
        classifier.set_classes(all_claases_in_model, filtered_classes)

        classes_dict = {}
        model_name = self.get_name()
        for klass in all_claases_in_model:
            classes_dict[klass] = rcnn_classes.get_thresh(model_name, klass)

        classifier.set_classes_threshold(classes_dict)

        params = rcnn_classes.get_model_params(ds)

        classifier.set_thresholds(nms_threshold=params['nms_threshold'],
                                  min_box_size_percent=params['min_box_size_percent'])  # take the model params


# TODO faces remove after we check that no 'faces' detection is necessary
# class FaceDS(DataSet):
#     def load(self):
#         self.clasifier = new_face_client.load()


class CnnDS(DataSet):
    def load(self):
        print ("============<><><> Loading dataset(CNN) %s  on env (%s) <><><><>============" % (self.ds, self.env))
        model_dir = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.ds)
        self.clasifier = cnn.load_net(self.ds, model_dir)


class RcnnDS(DataSet):
    def load(self):
        print ("============<><><> Loading dataset %s  on env (%s) <><><><>============" % (self.ds, self.env))
        prototxt = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.env, self.ds, rcnn_classes.dataset_files[0])
        caffemodel = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.env, self.ds, rcnn_classes.dataset_files[1])
        self.clasifier = frcnn_net.load_net(self.ds, prototxt.encode('utf8'), caffemodel.encode('utf8'), self.gpu_id)
        #self.gop_service = gop_service.load()
        self.reload_classes()


    def do_im_detect(self, image_name, mat_file, svm=False):
        return self.clasifier.do_im_detect(image_name, mat_file, self.op_type, svm)



class TensorDS(DataSet):

    def load(self):
        from lib.algo.models import tensorf
        print ("============<><><> Loading tensorflow dataset %s  on env (%s) <><><><>============" % (self.ds, self.env))
        prefix_dir = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.env, self.ds)
        #prototxt = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.env, self.ds, rcnn_classes.dataset_files[0])
        #caffemodel = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.env, self.ds, rcnn_classes.dataset_files[1])

        self.clasifier = tensorf.load_net_obj(self.ds, prefix_dir.encode('utf8')) #, self.gpu_id)


    def do_im_detect(self, images_name):
        if type(images_name) is str:
            images_name = [images_name]
        return self.clasifier.extract_im_result(images_name)


class FasterDS(DataSet):

    def load(self):

        recog_ds, recog2_ds, source_ds, fourth_ds, ds = None, None, None, None, self.ds
        single_ds = True
        if ',' in self.ds:
            single_ds = False
            dss = []
            ds_list = self.ds.split(',')
            print ("============<><><> Loading %s datasets: %s  on env (%s) <><><><>============" % (len(ds_list), ds, self.env))
            for d in ds_list:
                dss.append(d.strip())
            ds = dss[0].strip()
            if len(dss) > 1:
                recog_ds = dss[1].strip()
            if len(dss) > 2:
                source_ds = dss[2].strip()
            if len(dss) > 3:
                recog2_ds = dss[3].strip()
            if len(dss) > 4:
                fourth_ds = dss[4].strip()

        if ds:
            if single_ds:
                print ("============<><><> Loading [faster] dataset %s  on env (%s) <><><><>============" % (ds, self.env))
                prototxt = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.env, ds, rcnn_classes.dataset_files[0])
                caffemodel = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.env, ds, rcnn_classes.dataset_files[1])
                self.clasifier = frcnn_net.load_net(ds, prototxt.encode('utf8'), caffemodel.encode('utf8'), self.gpu_id, faster_mode=True)
                self._reload_classes(ds)
            else:
                fs = get(ds, env=self.env, gpu_id=self.gpu_id)
                self.clasifier = fs.clasifier

        if recog_ds:
            recog_net = get(recog_ds, env=self.env, gpu_id=self.gpu_id)
            print ("============<><><> Loading  dataset %s  on env (%s) for recognition <><><><>============" % (recog_ds, self.env))
            #prototxt = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.env, recog_ds, rcnn_classes.dataset_files[0])
            #caffemodel = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.env, recog_ds, rcnn_classes.dataset_files[1])
            self.recog_clasifier = recog_net.clasifier # frcnn_net.load_net(recog_ds, prototxt.encode('utf8'), caffemodel.encode('utf8'),
            #self.gpu_id, faster_mode=True)
            if type(recog_net) == FasterDS:
                self._reload_classes(recog_ds, self.recog_clasifier)
        if source_ds:
            print ("============<><><> Loading  dataset %s  on env (%s) for sourcing <><><><>============" % (source_ds, self.env))
            source_net = get(source_ds, env=self.env, gpu_id=self.gpu_id)
            #prototxt = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.env, source_ds, rcnn_classes.dataset_files[0])
            #caffemodel = os.path.join(os.path.join(os.environ['HOME'], 'caffemodel'), self.env, source_ds, rcnn_classes.dataset_files[1])
            self.source_clasifier = source_net.clasifier #frcnn_net.load_net(source_ds, prototxt.encode('utf8'), caffemodel.encode('utf8'),
            #self.gpu_id, faster_mode=True)
            if type(source_net) == FasterDS:
                self._reload_classes(source_ds, self.source_clasifier)

        if recog2_ds:
            print ("============<><><> Loading  dataset %s  on env (%s) for adv recognition <><><><>============" % (recog2_ds, self.env))
            recog2_net = get(recog2_ds, env=self.env, gpu_id=self.gpu_id)
            self.recog2_clasifier = recog2_net.clasifier
            if type(recog2_net) == FasterDS:
                self._reload_classes(recog2_ds, self.recog2_clasifier)

        if fourth_ds:
            print ("============<><><> Loading  dataset %s  on env (%s) for fourth model <><><><>============" % (fourth_ds, self.env))
            fourth_model_net = get(fourth_ds, env=self.env, gpu_id=self.gpu_id)
            self.fourth_model = fourth_model_net.clasifier
            if type(self.fourth_model) == FasterDS:
                self._reload_classes(recog2_ds, self.recog2_clasifier)

    def do_im_detect_with_source_and_recog(self, image_name, stam=None):
        from lib.algo.clustering.test_proc_cluster_flow import vb_infer
        print ("do_im_detect_with_source_and_recog:", image_name)
        img = cv2.imread(image_name)
        no_vec = not self.recog_clasifier
        if self.fourth_model:
            # Note that the fourth model can be a recognition model as well
            detection_models = [self.clasifier, self.fourth_model]
        else:
            detection_models = [self.clasifier]
        result = vb_infer(img, detection_models, m_feat=self.recog_clasifier, m_rec_rcnn=self.recog2_clasifier)
        res = {}
        for b in result:
            if 'brand' in b:
                brand = b['brand']
                # b['scr'] = b['scr_rcnn']
                # del b['scr_rcnn']
                # del b['brand']
                if "scr_rcnn" in b:
                    b["rec_scr"] = b["scr_rcnn"]
            else:
                brand = 'new_class'

            if no_vec:
                b['vec'] = []

            if brand not in res:
                res[brand] = [b]
            else:
                res[brand].append(b)
        return res

    def do_im_detect(self, image_name, process_gl=None):
        print (image_name)
        img = cv2.imread(image_name)
        return self.clasifier.extract_im_result(img, process_gl=process_gl)

    def source_detection(self, img, box):
        return self.source_clasifier.image_source_detection(img, box)

    # TODO unused with dependencies
    # def brand_source_detect(self, vid, json_spot_path):
    #     v = VideoResult.objects.get(vid=vid)
    #     progress_func = v.set_progress
    #     return self.clasifier.spots_source(json_spot_path, vid,  progress_func)
    #
    # def extract_source_per_brand(self, brand, vid, spots_list):
    #     return self.clasifier.spots_brand_source(brand, vid, spots_list)

    def sourcing(self, spots_list):
        return self.clasifier.spots_source2(spots_list)


DS = {}


def get_refreshing_models(file):
    if os.path.isfile(file):
        with open(file) as f:
            data = json.load(f)
    else:
        data = set()

    return data

#Not used
def reload(dataset):
    print ("add %s to refresh cycle.." % dataset)
    data = get_refreshing_models(refresh_model_file)
    data.add(dataset)
    with open(refresh_model_file) as f:
        json.dump(data, f)

def need_db_refresh():
    gpu_no = get_no_of_gpu()
    for i in range(gpu_no):
        os.system("touch %s.%s" % (refresh_model_file, i))

def get(dataset,env='', gpu_id=None):
    if gpu_id is None:
        try:
            gpu_id = int(os.environ.get("GPU_HARD_ID"))
        except TypeError:
            gpu_id = None
    #LOGO_CLASSIFIER = logo_classifier_sift.LogoClassifier

    ds = dataset #TODO: do we need to support this

    print ("load dataset ====== %s ======" % ds)
    if ds in DS and DS[ds] is not None:
        file = "%s.%s" % (refresh_model_file, gpu_id)
        if os.path.exists(file):
            #models = get_refreshing_models(refresh_model_file)
            for model in DS:
                print ("reload model: %s" % model)
                DS[model].reload_classes()
            try:
                os.remove(file)
            except OSError:
                pass
        return DS[ds]

    if ds in rcnn_classes.get_cnn_models(cache=False):
        DS[ds] = CnnDS()
        DS[ds].set(dataset, env)

    elif ds in rcnn_classes.get_rcnn_models(cache=False):
        DS[ds] = RcnnDS()
        DS[ds].set(dataset, env)
        DS[ds].set_gpu(gpu_id)

    elif ',' in ds or ds in rcnn_classes.get_faster_models(cache=False):
        DS[ds] = FasterDS()
        DS[ds].set(dataset, env)
        DS[ds].set_gpu(gpu_id)

    elif ds in rcnn_classes.get_tf_models(cache=False):
        DS[ds] = TensorDS()
        DS[ds].set(dataset, env)
        DS[ds].set_gpu(gpu_id)

    # TODO faces remove after we check that no 'faces' detection is necessary
    # elif dataset == 'faces':
    #     DS[ds] = FaceDS()

    else:
        raise Exception("No such dataset named: %s, hostname=%s" % (ds, hostname.ip))

    DS[ds].load()

    return DS[ds]


