# coding: utf-8
#!/usr/bin/env python

# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------
from lib import _init_paths
import json
import numpy as np
import caffe
import os
import cv2


from fast_rcnn.config import cfg
from fast_rcnn.test import im_detect
from utils.timer import Timer

from lib.algo import mutils
from lib.algo.common import load_mat_file
from lib.algo.models.faster import FasterRcnnClassifier
from lib.algo.models.mutils import extract_human_result, filter_prop_by_size, get_best_score
from lib.algo.models.rcnn import LogosClasifier
from lib.algo.mutils import load_pkl_file
from lib.algo.selective import s3
from lib.algo import svm_classsifier_helper
import rcnn_classes

from lib.algo.models import rcnn as rcnn_classifier

VISUAL_BY_PASS = False

def load_cc_file(simple_file):
    pass


def load_net(ds_name, prototxt_file, caffemodel_file, gpu_id=None, faster_mode=False):
    gpu_id = mutils.dicover_gpu_by_task_id() if gpu_id is None else gpu_id

    #TODO: #os.environ['CUDA_VISIBLE_DEVICES'] = str(gpu_id)

    if not os.path.isfile(prototxt_file):
        raise IOError(('{:s} not found.').format(prototxt_file))

    if not os.path.isfile(caffemodel_file):
        raise IOError(('{:s} not found.').format(caffemodel_file))

    print ("loading %s " % prototxt_file)

    print ("************************* set GPU_ID = %s ************************" % gpu_id)

    caffe.set_mode_gpu()
    if faster_mode:
        caffe.set_device(gpu_id)
        cfg.GPU_ID = gpu_id

    net = caffe.Net(prototxt_file, caffemodel_file, caffe.TEST)
    print ('\n\nLoaded network {:s}'.format(caffemodel_file))

    if faster_mode:
        obj = FasterRcnnClassifier(net, ds_name)
    elif ds_name in ["logos", "nude"]:
        obj = LogosClasifier(net, ds_name)
    else:
        obj = RcnnClassifierExtender(net, ds_name)

    #obj.set_gpu(gpu_id)

    return obj


def load_tf_net(ds_name, prototxt_file, caffemodel_file, gpu_id=None, faster_mode=False):
    gpu_id = mutils.dicover_gpu_by_task_id() if gpu_id is None else gpu_id

    #TODO: #os.environ['CUDA_VISIBLE_DEVICES'] = str(gpu_id)

    if not os.path.isfile(prototxt_file):
        raise IOError(('{:s} not found.').format(prototxt_file))

    if not os.path.isfile(caffemodel_file):
        raise IOError(('{:s} not found.').format(caffemodel_file))

    print ("loading %s " % prototxt_file)

    print ("************************* set GPU_ID = %s ************************" % gpu_id)

    caffe.set_mode_gpu()
    if faster_mode:
        caffe.set_device(gpu_id)
        cfg.GPU_ID = gpu_id

    net = caffe.Net(prototxt_file, caffemodel_file, caffe.TEST)
    print ('\n\nLoaded network {:s}'.format(caffemodel_file))

    obj = FasterRcnnClassifier(net, ds_name)

    return obj





class RcnnClassifierExtender(rcnn_classifier.RcnnClassifier):

    def do_im_detect_for_svm(self, image_name, obj_proposals, svm_mode=1):
        # Load the demo image
        im_file = image_name
        im = cv2.imread(im_file)
        print(im.shape)

        obj_proposals = filter_prop_by_size(obj_proposals , im.shape , size_th = self.min_box_size_percent)
        #print "porposals after ratio filter" , len(obj_proposals)
        obj_proposals = s3.nms2(obj_proposals, self.nms_thresh)
        # Detect all object classes and regress object bounds
        timer = Timer()
        timer.tic()
        scores, boxes = im_detect(self.net, im, obj_proposals)

        voting_frame = svm_classsifier_helper.voting_per_frame(scores)

        timer.toc()
        print (('Detection took {:.3f}s for '
               '{:d} object proposals').format(
               timer.total_time, boxes.shape[0]))

        print ("svm_mode = %s" % svm_mode)
        if VISUAL_BY_PASS or svm_mode == 1:
            final_res = dict(classify={}, vector=voting_frame)
            return final_res
        # Visualize detections for each class

        result = extract_human_result(self, self.relevant_clasess,self.clasess, scores,boxes)

        final_res = dict(classify=result, vector=voting_frame)

        return final_res


    def do_im_detect(self, image_name, mat_file, box_type='ss', svm=0):
        if os.stat(mat_file).st_size == 0:
            raise "Mat file (%s) is empty" % mat_file

        if box_type == 'ss':
            obj_proposals = load_mat_file(mat_file)

            obj_proposals = obj_proposals[:, [1, 0, 3, 2]].tolist()
        elif box_type == 'gop' or box_type == 'cc':
            obj_proposals = load_pkl_file(mat_file)
        else:
            raise "No such box_type %s" % box_type
        if not svm:
            return self.do_detect(image_name, obj_proposals)
        else:
            return self.do_im_detect_for_svm(image_name, obj_proposals, svm)



def cell_test6(net, ds_name, im_file, classes, relevant_classes, nms_thresh=0.3):
    from utils.cython_nms import nms
    im = cv2.imread(im_file)
    gray = cv2.cvtColor(im , cv2.COLOR_RGB2GRAY)
    acktorgb = cv2.cvtColor(gray,cv2.COLOR_GRAY2RGB)
    scores, boxes = im_detect(net, acktorgb)
    results = {}
    for cls_ind, cls in enumerate(relevant_classes):
        cls_ind = classes.index(cls) + 1
        cls_boxes = boxes[:, 4*cls_ind:4*(cls_ind + 1)]
        cls_scores = scores[:, cls_ind]
        dets = np.hstack((cls_boxes, cls_scores[:, np.newaxis])).astype(np.float32)
        keep = nms(dets, nms_thresh)
        dets = dets[keep, :]
        results[cls] = get_best_score(dets, thresh=rcnn_classes.get_thresh(ds_name,cls))
    return results


def load_faster(gpu_id=0):
    cfg.TEST.HAS_RPN = True
    gpu_id = 0
    prototxt = '/home/ubuntu/caffemodel/flyers/test.prototxt'
    caffemodel = '/home/ubuntu/caffemodel/flyers/caffemodel'
    caffe.set_mode_gpu()
    caffe.set_device(gpu_id)
    cfg.GPU_ID = gpu_id
    net = caffe.Net(prototxt, caffemodel, caffe.TEST)
    return net
