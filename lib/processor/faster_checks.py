import os
from config import REPO_DIR


def check_model_files(models_names, env=None):
    if env is None:
        env = os.environ.get("ENV", "production")
    for model_name in models_names:
        # Empty model name should not be checked
        if not model_name:
            continue
        repo_path = os.path.join(REPO_DIR, env, model_name)
        if not os.path.exists(repo_path):
            return False
    return True
