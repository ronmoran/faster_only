__author__ = 'hagay'

import os
import sys
import threading
import time
import traceback
from shutil import copyfile

import config
from lib import hostname, datasets


class SSprocessingThread(threading.Thread):
    def __init__(self, vid, img_name, op_model='ss'):
        threading.Thread.__init__(self)
        self.video_id = vid
        self.img_name = img_name
        self.op_model = op_model
        self.final_ret = None
        self.done = False
        self.elapsed_time = 0
        self.exception = None

    def set_objects(self, s3con, ss_obj):
        self.s3con = s3con
        self.ss_obj = ss_obj

    def get_result(self):
        return self.final_ret

    def isDone(self):
        return self.done

    def run(self):

        start_time = time.time()
        try:
            if config.file_protocol == 's3':
                s3_file = "%s/%s.jpg" % (self.video_id, self.img_name)
                imagefile = "%s/%s/%s.jpg" % (config.LOCAL_STORAGE, self.video_id, self.img_name)
                self.s3con.download(s3_file, imagefile)
            else:
                imagefile = "%s/%s/%s.jpg" % (config.nfs_media_dir, self.video_id, self.img_name)

            start_time = time.time()
            mat_file = self.ss_obj.do_ss(imagefile, stype=self.op_model)
            print ("return : mat_file = %s" % mat_file)
            elapsed_time = time.time() - start_time
            if config.file_protocol == 's3':
                dest_mat_file = "%s/%s.mat" % (self.video_id, self.img_name)
                self.s3con.upload(mat_file, dest_mat_file)
            else:
                copyfile(mat_file, '%s/%s/%s.mat' % (config.nfs_media_dir, self.video_id, self.img_name))

            print ("SS %s/%s took ..%s" % (self.video_id, imagefile, elapsed_time))
            self.final_ret = dict(status=True, elapsed_time=elapsed_time, ip=hostname.ip)
        except Exception as e:
            traceback.print_exc()
            self.final_ret = dict(status=False, ip=hostname.ip, reason=e)
            self.exception = sys.exc_info()
            raise e

        finally:
            self.elapsed_time = round(time.time() - start_time, 1)
            self.done = True


class RcnnServerThread(threading.Thread):
    def __init__(self, dataset, vid, img_names, op_type, is_svm=0):
        threading.Thread.__init__(self)
        self.dataset = dataset
        self.video_id = vid
        self.img_names = img_names
        self.env = ''
        self.dataset = dataset.encode('utf8')
        self.is_svm = is_svm
        self.status = True
        self.options = None
        self.elapsed_time = 0

        # We need to know the object proposal type in order to read the boxes
        self.op_type = op_type

        self.final_ret = {}
        self.done = False
        self.__exception = None
        self.__exception_lock = threading.Lock()

    def set_options(self, options):
        self.options = options

    def set_objects(self, s3con, ss_obj=None):
        self.s3con = s3con
        self.ss_obj = ss_obj

    def set_exc(self, exc_value):
        """
        Set the exception value safely, to avoid having the exception changed by another thread.
        :param None exc_value: A tuple as returned by sys.exc_info() or None
        """
        with self.__exception_lock:
            self.__exception = exc_value

    def remove_exc(self):
        """
        Remove an exception safely (without removing newly-written exceptions).
        """
        if self.__exception_lock.locked():
            return
        self.set_exc(None)

    def has_exc(self):
        if self.__exception is not None:
            return True

    def get_exc(self):
        return self.__exception

    def get_result(self):
        return self.final_ret

    def isDone(self):
        return self.done

    def set_env(self, env):
        self.env = env

    def _handle_exc(self, error):
        """
        :param Exception error:
        """
        traceback.print_exc()
        self.final_ret = "%s: Fail to perform rcnn, Reason %s" % (hostname.ip, traceback.format_exc())
        self.status = False
        self.set_exc(sys.exc_info())
        raise error

    def _finalize(self, start_time):
        self.elapsed_time = round(time.time() - start_time, 1)
        self.done = True

    def runit(self):
        if config.file_protocol == 's3':
            jpeg_files = []
            mat_files = []
            for img_name in self.img_names:
                mat_file = "%s/%s_%s.mat" % (config.local_media_dir, self.video_id, img_name)
                src_mat_file = "%s/%s.mat" % (self.video_id, img_name)

                jpeg_file = "%s/%s_%s.jpg" % (config.local_media_dir, self.video_id, img_name)
                src_jpeg_file = "%s/%s.jpg" % (self.video_id, img_name)

                s3_start_time = time.time()
                self.s3con.download(src_mat_file, mat_file)
                self.s3con.download(src_jpeg_file, jpeg_file)
                jpeg_files.append(jpeg_file)
                mat_files.append(mat_file)
                s3_elappsed_time = round(time.time() - s3_start_time, 1)

                print ("-------> S3 download time: %s" % s3_elappsed_time)


        net = datasets.get(self.dataset, self.env)
        net.set_op_type(self.op_type)

        for img_name in self.img_names:
            jpeg_file = "%s/%s/%s.jpg" % (config.nfs_media_dir, self.video_id, img_name)
            mat_file =  "%s/%s/%s.mat" % (config.nfs_media_dir, self.video_id, img_name)

            if not os.path.exists(jpeg_file):
                raise Exception("files jpeg not found: %s" % jpeg_file)
            if not os.path.exists(mat_file):
                raise Exception("files mat not found: %s" % mat_file)

            rcnn_start_time = time.time()
            self.final_ret[img_name] = net.do_im_detect(jpeg_file, mat_file, self.is_svm)
            rcnn_elappsed_time = round(time.time() - rcnn_start_time, 1)

            print ("-------> rcnn  time: %s" % rcnn_elappsed_time)


    def run(self):

        start_time = time.time()
        try:
            self.runit()
        except Exception as e:
            self._handle_exc(e)

        finally:
            self._finalize(start_time)


class FasterServerThread(RcnnServerThread):
    def __init__(self, dataset, vid, img_names, op_type, is_svm=0):
        super(FasterServerThread, self).__init__(dataset, vid, img_names, op_type, is_svm)

    def runit(self):
        if config.file_protocol == 's3':
            jpeg_files = []
            for img_name in self.img_names:
                jpeg_file = "%s/%s_%s.jpg" % (config.local_media_dir, self.video_id, img_name)
                src_jpeg_file = "%s/%s.jpg" % (self.video_id, img_name)

                s3_start_time = time.time()
                self.s3con.download(src_jpeg_file, jpeg_file)
                jpeg_files.append(jpeg_file)
                s3_elappsed_time = round(time.time() - s3_start_time, 1)

                print ("-------> S3 download time: %s" % s3_elappsed_time)

        print ("**** [%s]loading dataset: %s" % (self.env, self.dataset))
        net = datasets.get(self.dataset, self.env)
        do_im_detect = net.do_im_detect_with_source_and_recog if ',' in self.dataset else net.do_im_detect
        #print ("--%s--" % self.options)
        gl_options = self.options and 'gl' in self.options

        print ("XX:processing %s images" % len(self.img_names))

        idx=0
        prefix=self.img_names[0] if len(self.img_names) > 0 else ''
        for img_name in self.img_names:
            rcnn_start_time = time.time()
            jpeg_file = "%s/%s/%s.jpg" % (config.nfs_media_dir, self.video_id, img_name)
            idx += 1
            print ("XX-%s:%s : %s" % (prefix, idx, jpeg_file))
            if not os.path.exists(jpeg_file):
                print ("files path not found: %s" % jpeg_file)
                continue

            jpeg_gl_file = "%s/%s/%s_gl.jpg" % (config.nfs_media_dir, self.video_id, img_name) if gl_options else None

            try:
                self.final_ret[img_name + ".jpg"] = do_im_detect(jpeg_file, jpeg_gl_file)

                rcnn_elappsed_time = round(time.time() - rcnn_start_time, 1)

                print ("-------> faster  time: %s" % rcnn_elappsed_time)
            except IndexError as ex:
                traceback.print_exc()
                print ("Yakir's error: %s" % ex)
