#!/bin/bash
s=$(grep FACTOR /home/ubuntu/.vbrand_rcnn || echo "f=1")
factor=$(echo $s | cut -d= -f2)
gpu_cards=$(expr $(/usr/bin/nvidia-smi | grep "Default" | wc -l) \* $factor)
echo $gpu_cards
